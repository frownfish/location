#! /usr/bin/python

import csv
import os
import sys
from typing import List
from datetime import datetime, timedelta
from math import cos, asin, sqrt, pi
import random


speed = 8 * 5280 / 3600  # ft/s
earth_radius = 3963 * 5280  # ft
time_per_waypoint = 1.0
waggle = 0.000005

def interpolate(lat1, lon1, lat2, lon2, duration):
  points = []
  count = max(int(duration / time_per_waypoint), 1)

  for i in range(0, count):
    lat = lat1 + (lat2 - lat1) * (i / count) + waggle * random.uniform(-1, 1)
    lon = lon1 + (lon2 - lon1) * (i / count) + waggle * random.uniform(-1, 1)
    points.append(waypoint(lat, lon))
  return points

def distance(lat1, long1, lat2, long2):
    p = pi / 180.0
    a = 0.5 - cos((lat2 - lat1) * p)/2 + cos(lat1 * p) * \
        cos(lat2 * p) * (1 - cos((long2 - long1) * p)) / 2
    return 2 * earth_radius * asin(sqrt(a))

class waypoint:
  def __init__(self, latitude, longitude):
    self.latitude = latitude
    self.longitude = longitude

  def __str__(self):
    return f'<wpt lat="{self.latitude}" lon="{self.longitude}"></wpt>'

  __repr__ = __str__

class gpxfile:
  def __init__(self, waypoints: List[waypoint]):
    self.waypoints = waypoints

  def __str__(self):
    waypoints = '\n'.join([str(x) for x in self.waypoints])
    return f"""<?xml version="1.0"?>
<gpx version="1.1" creator="Xcode">
{waypoints}
</gpx>
    """

if __name__ == '__main__':
  waypoints = []
  time = datetime.now()

  assert(len(sys.argv) == 3)

  infile = sys.argv[1]
  outfile = sys.argv[2]

  with open(infile, 'r') as f:
      reader = csv.DictReader(f)

      for row in reader:
        latitude = float(row['latitude'])
        longitude = float(row['longitude'])
        duration = int(row['duration'] or 0)
        name = row['name']

        if len(waypoints) > 0:
          last = waypoints[-1]
          dist = distance(latitude, longitude, last.latitude, last.longitude)
          delta = dist / speed
          waypoints.extend(
            interpolate(last.latitude, last.longitude, latitude, longitude, delta)
          )
        else:
          waypoints.append(waypoint(latitude, longitude))

        if duration > 0:
          waypoints.extend(
            interpolate(latitude, longitude, latitude, longitude, duration)
          )

  file = gpxfile(waypoints)

  with open(outfile, 'w') as f:
    f.write(str(file))
